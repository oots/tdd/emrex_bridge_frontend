# EMREX Bridge Frontend

> Frontend app for that connects directly with EMREX

## Table of Contents

1. [Description](#description)
2. [Getting started](#getting-started)
3. [Prerequisites](#prerequisites)
4. [Installation](#installation)
4. [Running the app](#running-the-app)
5. [Licensing](#licensing)

## Description

This app is a frontend application which shows an UI that allows the user to interact with `EMREX`.

## Getting started

In order to run the project it is needed to create a `.env` file locally.

Variables to fill:

- `NEXT_PUBLIC_BRIDGE_URL`: OOTS - EMREX Bridge URL (Backend)
- `NEXT_PUBLIC_PROCEDURE_PORTAL`: OOTS - EMREX Bridge Frontend URL

## Prerequisites

- [Node.js](https://nodejs.org/en/download/)

## Installation

For the installation of this project it is recommended to have [nvm](https://github.com/nvm-sh/nvm) installed. In case you don't want to install `nvm`, just make sure to use the `NodeJS` version that appears in file `.nvmrc`.

```bash
$ nvm use
$ yarn install
```

## Running the app

### Run the project locally

```bash
$ yarn dev
```

### Run with docker

```bash
$ docker-compose up --build
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


## License

This software is licensed under [European Union Public License (EUPL) version 1.2.](https://code.europa.eu/oots/tdd/oots_ex/-/blob/main/LICENSE)


