const options = [
  { value: '', label: 'Please select an option' },
  {
    value: 'https://ext-partner.his.de:8888/hisinone/api/v1/cs/psv/emrex',
    label:
      '[DE] HIS eG test EMP (ext-partner) (https://ext-partner.his.de:8888/hisinone/api/v1/cs/psv/emrex)',
  },
  {
    value: 'https://demo.campus.uni-freiburg.de/qisserver/api/v1/cs/psv/emrex',
    label:
      '[DE] Albert-Ludwigs-Universitaet Freiburg im Breisgau (EMREG-Test) (https://demo.campus.uni-freiburg.de/qisserver/api/v1/cs/psv/emrex)',
  },
  {
    value: 'https://vt-mijn.duo.nl/services/dr-iud/emrex/',
    label:
      '[NL] Netherlands test EMP (https://vt-mijn.duo.nl/services/dr-iud/emrex/)',
  },
  {
    value: 'https://vip-test.uio.no/vp/init',
    label: '[NO] Norway registry (https://vip-test.uio.no/vp/init)',
  },
];

export default options;
