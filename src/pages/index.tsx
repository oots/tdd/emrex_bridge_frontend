import { AttachmentElement, Elmo } from '@/types/elmo';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { MoonLoader } from 'react-spinners';
import useSWR, { Fetcher, Key } from 'swr';
import { xml2json } from 'xml-js';
import Button from '../components/Button';
import styles from '../styles/Home.module.scss';
import options from '../constants/constants';

const fetcher: Fetcher<any, Key> = (input: RequestInfo, init?: RequestInit) =>
  fetch(input, init).then((res) => res.json());

const getTitle = (attachment: AttachmentElement, index: number) => {
  // Check if is array
  if (!Array.isArray(attachment.title)) {
    return attachment.title._text;
  }

  // Try  to find english
  const title = attachment.title.find(
    (title) => title._attributes['xml:lang'] === 'en'
  );

  if (title) {
    return title._text;
  }

  if (attachment.title.length) {
    return attachment.title[0]._text;
  }

  return `Attachment ${index + 1}`;
};

export default function Home() {
  const { query } = useRouter();
  const [isReturnUrlSet, setIsReturnUrlSet] = useState<boolean>(false);
  const [elmo, setElmo] = useState<string>('');
  const [attachments, setAttachments] = useState<AttachmentElement[]>([]);
  const [frontend1URL, setFrontend1URL] = useState<string>();
  const [state, setState] = useState<number>(0);
  const [option, setOption] = useState<any>();
  const [loading, setLoading] = useState<boolean>(true);

  const { data } = useSWR<any, unknown>(
    query.sessionId
      ? [`${process.env.NEXT_PUBLIC_BRIDGE_URL}/preview/` + query.sessionId]
      : null,
    fetcher,
    {
      refreshInterval: 10010,
    }
  );

  useEffect(() => {
    if (data && data.data && !elmo) {
      setElmo(data.data.elmo);
    }
    if (elmo && state < 2) {
      setState(2);
      const payload: Elmo = JSON.parse(
        xml2json(elmo, {
          compact: true,
          spaces: 4,
        })
      );

      if (payload.elmo.attachment != null) {
        setAttachments(
          Array.isArray(payload.elmo.attachment)
            ? payload.elmo.attachment
            : [payload.elmo.attachment]
        );
      } else if (payload.elmo.report && payload.elmo.report.attachment) {
        setAttachments(
          Array.isArray(payload.elmo.report.attachment)
            ? payload.elmo.report.attachment
            : [payload.elmo.report.attachment]
        );
      }
    }
    if (!elmo && state < 1) {
      setState(1);
    }
  }, [data, elmo, state]);

  const approveTransfer = async () => {
    setState(3);
    let reqData = {
      previewId: query.sessionId,
      approve: true,
    };

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(reqData),
    };

    const res = await fetch(
      `${process.env.NEXT_PUBLIC_BRIDGE_URL}/preview`,
      requestOptions
    );
    const resData = await res.json();

    if (res.status === 200) {
      setFrontend1URL(query.returnurl as string);
      setTimeout(() => setState(4), 5000);
    }
  };

  const declineTransfer = async () => {
    setState(3);
    let reqData = {
      previewId: query.sessionId,
      approve: false,
    };

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(reqData),
    };

    const res = await fetch(
      `${process.env.NEXT_PUBLIC_BRIDGE_URL}/preview`,
      requestOptions
    );

    const resData = await res.json();

    if (res.status === 200) {
      setFrontend1URL(query.returnurl as string);
      setTimeout(() => setState(4), 5000);
    }
  };

  const sendErrorMessage = async () => {
    setState(3);
    let reqData = {
      previewId: query.sessionId,
      error: true,
    };

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(reqData),
    };

    const res = await fetch(
      `${process.env.NEXT_PUBLIC_BRIDGE_URL}/preview`,
      requestOptions
    );

    const resData = await res.json();

    if (res.status === 200) {
      setFrontend1URL(query.returnurl as string);
      setTimeout(() => setState(4), 5000);
    }
  };

  useEffect(() => {
    const updateReturnUrl = async () => {
      await fetch(`${process.env.NEXT_PUBLIC_BRIDGE_URL}/returnUrl`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          previewId: query.sessionId,
          returnUrl: query.returnurl,
        }),
      });
    };

    if (!isReturnUrlSet && query.sessionId && query.returnurl) {
      setIsReturnUrlSet(true);
      updateReturnUrl().catch((err) => console.log(err));
    }
  }, [isReturnUrlSet, query]);

  useEffect(() => {
    // Set loading false after 1 second
    setTimeout(() => setLoading(false), 1000);
  }, []);

  const defaultOption = options[0];

  return (
    <>
      <Head>
        <title>OOTS Emrex Brdige</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="flex min-h-screen flex-col items-center justify-center bg-gradient-to-br from-indigo-200 to-pink-200 p-24">
        <h1 className="m-10 text-4xl">EMREX Bridge Frontend</h1>
        {loading && <MoonLoader className="m-5" color="#000" />}
        {!loading && (
          <>
            {query.sessionId &&
              query.returnurl &&
              !query.NCP_CANCEL &&
              !query.NCP_NO_RESULTS &&
              !query.NCP_ERROR &&
              state === 1 && (
                <div className={styles.dropdownDiv}>
                  <Dropdown
                    className={styles.dropdown}
                    options={options}
                    onChange={(e) => setOption(e.value)}
                    value={defaultOption}
                    placeholder="Select an option"
                  />
                  <form action={option} method="POST">
                    <input
                      type="hidden"
                      name="sessionId"
                      value={(query.sessionId as string).replace(/-/g, '')}
                    />
                    <input
                      type="hidden"
                      name="returnUrl"
                      value="https://20.103.252.46:3001/store"
                    />
                    <button
                      type="submit"
                      className="m-4 cursor-pointer rounded-xl bg-lime-100 p-3"
                    >
                      Log in
                    </button>
                  </form>
                </div>
              )}
            {!query.sessionId && (
              <div>
                <h2>Missing session id</h2>
              </div>
            )}
            {!query.returnurl && (
              <div>
                <h2>Missing return url</h2>
              </div>
            )}
            {(query.NCP_CANCEL || query.NCP_NO_RESULTS) && state < 3 && (
              <div className="flex space-x-6">
                <button
                  className="m-4 rounded-xl bg-lime-100 p-3"
                  onClick={() => declineTransfer()}
                >
                  Decline transfer
                </button>
              </div>
            )}
            {query.NCP_ERROR && state < 3 && (
              <div className="flex space-x-6">
                <button
                  className="m-4 rounded-xl bg-lime-100 p-3"
                  onClick={() => sendErrorMessage()}
                >
                  Return to Procedure portal
                </button>
              </div>
            )}
            {attachments.length > 0 && state === 2 && (
              <div>
                <table className="table-auto">
                  <thead>
                    <tr>
                      <th className="px-4 py-2">Name</th>
                      <th className="px-4 py-2">Type</th>
                      <th className="px-4 py-2">Size</th>
                      <th className="px-4 py-2">Download</th>
                    </tr>
                  </thead>
                  <tbody>
                    {attachments.map((attachment, i) => {
                      return (
                        <tr key={i}>
                          <td className="border px-4 py-2">
                            {getTitle(attachment, i)}
                          </td>
                          <td className="border px-4 py-2">PDF</td>
                          <td className="border px-4 py-2">
                            {(
                              attachment.content._text.length /
                              (1024 * 1024)
                            ).toPrecision(2)}{' '}
                            MB
                          </td>
                          <td className="border px-4 py-2">
                            <a
                              href={attachment.content._text}
                              download={
                                'emrex-data-' + new Date().getTime() + '.pdf'
                              }
                            >
                              Download PDF
                            </a>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            )}
            {state === 2 && (
              <div className="flex space-x-6">
                <button
                  className="m-4 rounded-xl bg-lime-100 p-3"
                  onClick={() => approveTransfer()}
                >
                  Approve transfer
                </button>
                <button
                  className="m-4 rounded-xl bg-lime-100 p-3"
                  onClick={() => declineTransfer()}
                >
                  Decline transfer
                </button>
              </div>
            )}
            {state === 3 && (
              <div className="flex flex-col items-center justify-center">
                <p>Waiting for response...</p>
                <MoonLoader className="m-5" color="#000" />
              </div>
            )}
            {state === 4 && (
              <div>
                <Button variant="primary" className="bg-green-300">
                  <a href={frontend1URL}>Go to procedure portal </a>
                </Button>
              </div>
            )}
          </>
        )}
      </main>
    </>
  );
}
